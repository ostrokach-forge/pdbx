#!/bin/bash

set -ev

export ACLOCAL_FLAGS="-I$PREFIX/share/aclocal"
export PKG_CONFIG_PATH="$PREFIX/lib/pkgconfig:$PREFIX/share/pkgconfig"

export CC="/opt/rh/devtoolset-7/root/usr/bin/gcc"
export CXX="/opt/rh/devtoolset-7/root/usr/bin/g++"

# Common
export CPPFLAGS="${CPPFLAGS} -I${PREFIX}/include"
export LDFLAGS="${LDFLAGS} -L${PREFIX}/lib"
export CFLAGS="${CFLAGS} -m${ARCH}"
export CXXFLAGS="${CXXFLAGS} -m${ARCH}"

# Linux only
export CXXFLAGS="${CXXFLAGS} -DBOOST_MATH_DISABLE_FLOAT128"
export LDFLAGS="${LDFLAGS} -Wl,-rpath,$PREFIX/lib" 
export LINKFLAGS="${LDFLAGS}"

export PATH="/opt/rh/devtoolset-7/root/usr/bin:${PATH}"

# Dependencies have corrupt libtool files, leading to error:
# `../lib/libiconv.la' is not a valid libtool archive`
# rm -f $PREFIX/lib/*.la

mkdir -p build
cd build
cmake \
    -DCMAKE_INSTALL_PREFIX:PATH="${PREFIX}" \
    -DCMAKE_INSTALL_LIBDIR:PATH="${PREFIX}/lib" \
    -DCMAKE_BUILD_TYPE=RelWithDebInfo \
    ..
make
make install
